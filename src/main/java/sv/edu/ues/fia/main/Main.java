/*
 * Copyleft - 08-04-2015
 * This file (Main.java), can be shared and/or modified, by anybody.
 * El Salvador - Kevin Rivera <kevinrev26@gmail.com>
 * ==========================================================
 */
package sv.edu.ues.fia.main;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import sv.edu.ues.fia.cdibeans.*;
/**
 *
 * @author Kevin Rivera <kevinrev26@gmail.com>
 */
public class Main {
    
    public static void main(String[] args) {
        Weld instancia = new Weld();
        WeldContainer contenedor = instancia.initialize();
        LibroService servicio = contenedor.instance().select(LibroService.class).get();    
        Libro libro = servicio.crearLibro("Java EE 7-Beginning", 25.75, "A brief explanation about..");
        servicio.imprimir(libro);
        instancia.shutdown();
    }
}
