/*
 * Copyleft - 08-04-2015
 * This file (GeneradorNumero.java), can be shared and/or modified, by anybody.
 * El Salvador - Kevin Rivera <kevinrev26@gmail.com>
 * ==========================================================
 */
package sv.edu.ues.fia.interfaces;

/**
 *
 * @author Kevin Rivera <kevinrev26@gmail.com>
 */
public interface GeneradorNumero {
    public String generarNumero();
}
