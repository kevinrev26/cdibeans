/*
 * Copyleft - 08-04-2015
 * This file (TreceDigitos.java), can be shared and/or modified, by anybody.
 * El Salvador - Kevin Rivera <kevinrev26@gmail.com>
 * ==========================================================
 */
package sv.edu.ues.fia.qualifiers;

import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import javax.inject.Qualifier;
import java.lang.annotation.Target;
import static java.lang.annotation.ElementType.*;

/**
 *
 * @author Kevin Rivera <kevinrev26@gmail.com>
 */
@Qualifier
@Retention(RUNTIME)
@Target({FIELD,TYPE,METHOD})
public @interface TreceDigitos {
    
}
