/*
 * Copyleft - 08-04-2015
 * This file (OchoDigitos.java), can be shared and/or modified, by anybody.
 * El Salvador - Kevin Rivera <kevinrev26@gmail.com>
 * ==========================================================
 */
package sv.edu.ues.fia.qualifiers;

import javax.inject.Qualifier;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 *
 * @author Kevin Rivera <kevinrev26@gmail.com>
 */
@Qualifier
@Retention(RUNTIME)
@Target({FIELD, TYPE, METHOD})
public @interface OchoDigitos {
    
}
