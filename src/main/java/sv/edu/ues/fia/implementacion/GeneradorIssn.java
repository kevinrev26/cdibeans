/*
 * Copyleft - 08-04-2015
 * This file (GeneradorIssn.java), can be shared and/or modified, by anybody.
 * El Salvador - Kevin Rivera <kevinrev26@gmail.com>
 * ==========================================================
 */
package sv.edu.ues.fia.implementacion;

import java.util.Random;
import sv.edu.ues.fia.interfaces.GeneradorNumero;
import sv.edu.ues.fia.qualifiers.OchoDigitos;
/**
 *
 * @author Kevin Rivera <kevinrev26@gmail.com>
 */

@OchoDigitos
public class GeneradorIssn implements GeneradorNumero{

    public String generarNumero() {
        String isnn = "8-"+Math.abs(new Random().nextInt());
        return isnn;
    }
    
    
}
