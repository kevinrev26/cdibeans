/*
 * Copyleft - 08-04-2015
 * This file (GeneradorIsbn.java), can be shared and/or modified, by anybody.
 * El Salvador - Kevin Rivera <kevinrev26@gmail.com>
 * ==========================================================
 */
package sv.edu.ues.fia.implementacion;

import java.util.Random;
import sv.edu.ues.fia.interfaces.GeneradorNumero;
import sv.edu.ues.fia.qualifiers.TreceDigitos;
/**
 *
 * @author Kevin Rivera <kevinrev26@gmail.com>
 */

 @TreceDigitos
public class GeneradorIsbn implements GeneradorNumero{

    public String generarNumero() {
        String isbn = "13-84356-"+Math.abs(new Random().nextInt());
        return isbn;
    }
    
}
