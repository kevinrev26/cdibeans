/*
 * Copyleft - 08-04-2015
 * This file (LibroService.java), can be shared and/or modified, by anybody.
 * El Salvador - Kevin Rivera <kevinrev26@gmail.com>
 * ==========================================================
 */
package sv.edu.ues.fia.cdibeans;

import javax.inject.Inject;
import sv.edu.ues.fia.qualifiers.TreceDigitos;
import sv.edu.ues.fia.interfaces.GeneradorNumero;


/**
 *
 * @author Kevin Rivera <kevinrev26@gmail.com>
 */
public class LibroService {
    
    @Inject @TreceDigitos
    private GeneradorNumero gn;
    
    public Libro crearLibro(String titulo, double precio, String descripcion){
    Libro libro = new Libro(titulo,precio,descripcion);
    libro.setNumero(gn.generarNumero());
    return libro;
    }
    
    public void imprimir(Libro libro){
        System.out.println("**************************************");
        System.out.println("Nombre del libro: "+libro.getTitulo());
        System.out.println("Precio del libro: "+libro.getPrecio());
        System.out.println("Descripcion:  "+libro.getDescripcion());
        System.out.println("Numero de serie: "+libro.getNumero());
    }
}
