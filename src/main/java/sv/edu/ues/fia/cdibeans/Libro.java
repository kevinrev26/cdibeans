/*
 * Copyleft - 08-04-2015
 * This file (Libro.java), can be shared and/or modified, by anybody.
 * El Salvador - Kevin Rivera <kevinrev26@gmail.com>
 * ==========================================================
 */
package sv.edu.ues.fia.cdibeans;

/**
 *
 * @author Kevin Rivera <kevinrev26@gmail.com>
 */
public class Libro {
    private String titulo;
    private double precio;
    private String descripcion;
    private String numero;

    public Libro() {
    }

    public Libro(String titulo, double precio, String descripcion) {
        this.titulo = titulo;
        this.precio = precio;
        this.descripcion = descripcion;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }
    
    
}
